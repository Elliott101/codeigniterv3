<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
  

    public function dashboard(){
      $this->load->model('queries');
      $users=$this->queries->viewAllComputers(); // generating computer
      $this->load->view('dashboard',['users'=>$users]);
      if(!$this->session->userdata("user_id"))
      return redirect("welcome/login");

    }

    public function addCollege(){
       $this->load->view('addCollege');
    }
    
    public function createCollege(){
      $this->form_validation->set_rules('collegename','College Name','required');
      $this->form_validation->set_rules('branch','Branch','required');
      $this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
      if($this->form_validation->run()){
         $data=$this->input->post();
         $this->load->model('queries');
         if($this->queries->makeCollege($data)){
            $this->session->set_flashdata('message','College Created Successfully.');
         }else{
          $this->session->set_flashdata('message','Failed to Create College');
          
         }
         return redirect("admin/addCollege");

      }else{
         $this->addCollege();
      }
    }

    public function addCoadmin(){
      $this->load->model('queries');
      $roles = $this->queries->getRoles();
      $colleges = $this->queries->getColleges();
      $this->load->view('addCoadmin',['roles'=>$roles,'colleges'=>$colleges]);
    }

    public function createCoadmin(){
      $this->form_validation->set_rules('username','Username','required');
      $this->form_validation->set_rules('college_id','College Name','required');
      $this->form_validation->set_rules('email','Email','required');
      $this->form_validation->set_rules('gender','Gender','required');
      $this->form_validation->set_rules('role_id','Role','required');
      $this->form_validation->set_rules('password','Password','required');
      $this->form_validation->set_rules('confpwd','Password Again','required');

      $this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
      if($this->form_validation->run() ){
        $data = $this->input->post();
        $data['password'] =sha1($this->input->post('password')); // encrypting password
        $data['confpwd'] =sha1($this->input->post('confpwd'));
        $this->load->model('queries');	
        if($this->queries->registerCoadmin($data)){ // if data entered successfully return a value of 1
           $this->session->set_flashdata('message','CoAdmin Registered Successfully');
        }	
        else{
          $this->session->set_flashdata('message','Fail to Register Admin');
        }
        return redirect("admin/addCoadmin");
      }
      else{
        $this->addCoadmin();
    }
    }

    public function editComputer($device_id){
          $this->load->model('queries');
          $computerData = $this->queries->getComputerRecord($device_id);
          $parishes = $this->queries->getLocation();
          $this->load->view('edit_Computer',['computerData'=> $computerData,'parishes'=>$parishes]);
      }


      public function modifyComputer($device_id){
  $this->load->model('queries');
  $this->form_validation->set_rules('device_name','Device Name','required');
	$this->form_validation->set_rules('processor','Processor','required');
	$this->form_validation->set_rules('ram','Ram','required');
	$this->form_validation->set_rules('os_type','Operating System','required');
  $this->form_validation->set_rules('hardrive_size','Hard Drive Size','required');
  $this->form_validation->set_rules('parish_id','Location','required');
	$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

	if($this->form_validation->run() ){

		$data =array('device_name' => $this->input->post('device_name'),'processor' => $this->input->post('processor'),
		'ram' => $this->input->post('ram'),'os_type' => $this->input->post('os_type'),
		'hardrive_size' => $this->input->post('hardrive_size'),	'parish_id' => $this->input->post('parish_id')
	);
		$this->load->model('queries');	
		if($this->queries->updateComputer($data,$device_id)){ 
			 $this->session->set_flashdata('message','Computer Updated Successfully');
			 return redirect("admin/editComputer/{$device_id}");
		}	
		else{
			$this->session->set_flashdata('message','Fail to Update Computer');
			 return redirect("admin/editComputer/{$device_id}");

		}
	}
	
	else{
		$this->editComputer($device_id);
}
      }


      public function deleteComputer($device_id){
        $this->load->model('queries');	
        if($this->queries->removeComputer($device_id)){
          return redirect("admin/dashboard/{$device_id}");

        }
      }


    

    public function __construct(){
      parent::__construct();
      if(!$this->session->userdata("user_id")){
        return redirect("welcome/login");
      } 
    }
}