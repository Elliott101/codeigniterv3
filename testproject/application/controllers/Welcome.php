<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function index()
	{
		$this->load->model('queries');
		$chkAdminExist = $this->queries->chkAdminExist();
		$this->load->view('home', ['chkAdminExist' => $chkAdminExist]);
		
		//$data["viewAllComputers"] =$this->queries->viewAllComputers();
		//$this->load->view("welcome",$data);
	}

	public function filterComputers(){
		$this->load->model('queries');
        $getParishes = $this->queries->getParishes();
		$this->load->view('locationList', ['getParishes' => $getParishes]);

	}



	public function getRecords(){
		$this->load->model('queries');
		$parish = $this->input->post('parish_id');
		$getParishes = $this->queries->getParishes();
		$records = $this->queries->getRecords($parish);
		$this->load->view('getRecords',['getParishes' => $getParishes,'records'=>$records]);
	  }







	public function adminRegister(){
		$this->load->model('queries');
		$roles = $this->queries->getRoles();
		$this->load->view('register',['roles'=>$roles]);
	}

	    //-----------------
   
		public function computerLocations(){
			$this->load->model('queries');
			$parishes = $this->queries->getLocation();
			$this->load->view('addComputer',['parishes'=>$parishes]);
		}
	
	//------
	

	/*
	public function check_strong_password($str) // strong password validation
	
    {
       if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str) && preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $str)) {
		 return TRUE;
		
       }
       $this->form_validation->set_message('check_strong_password', 'The password field must contain at least one digit and one special character.');
       return FALSE;
	}
	*/
	public function valid_password($password = ''){
		$password = trim($password);

		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>ยง~]/';

		if (empty($password)){
			$this->form_validation->set_message('valid_password', 'The {field} field is required.');
			return FALSE;
		}

		if (preg_match_all($regex_lowercase, $password) < 1){
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one lowercase letter.');
			return FALSE;
		}

		if (preg_match_all($regex_uppercase, $password) < 1){
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one uppercase letter.');
			return FALSE;
		}

		if (preg_match_all($regex_number, $password) < 1){
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one number.');
			return FALSE;
		}

		if (preg_match_all($regex_special, $password) < 1){
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>ยง~'));
			return FALSE;
		}
		if (strlen($password) < 5){
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least 5 characters in length.');
			return FALSE;
		}

		if (strlen($password) > 32){
			$this->form_validation->set_message('valid_password', 'The {field} field cannot exceed 32 characters in length.');
			return FALSE;
		}
		return TRUE;
	}
	//strong password end




// checking if username exists
function checkUserName($userName){
	$this->load->model('queries');
	$username=$this->queries->checkusername($userName);
	if($username==false){
		return true;
	}
	else {
		$this->form_validation->set_message('checkUserName', 'This userName already exists!');
		return false;
	}
}

//checking if email exists
function checkEmail($email){
	$this->load->model('queries');
	$email=$this->queries->checkMail($email);
	if($email==false){
		return true;
	}
	else {
		$this->form_validation->set_message('checkEmail', 'This Email already exists!');
		return false;
	}
}


	public function adminSignup(){
		
		$this->form_validation->set_rules('username','Username','required|alpha_numeric|trim|callback_checkUserName');
		$this->form_validation->set_rules('email','Email','required|trim|valid_email|callback_checkEmail');
		$this->form_validation->set_rules('gender','Gender','required');
		$this->form_validation->set_rules('role_id','Role','required');
		$this->form_validation->set_rules('password','Password','required|min_length[8]|callback_valid_password');
		$this->form_validation->set_rules('confpwd','Confirm Password','required|matches[password]');
		$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

		if($this->form_validation->run() ){
			$this->load->library('encryption');
			$verfication_key =md5(rand());
			$encrypted_password = hash('sha256', 'x(93g'.$this->input->post("password").'4$b7@');
			$repeat_pass = hash('sha256', 'x(93g'.$this->input->post("password").'4$b7@');


			$data =array('username' => $this->input->post('username'),'email' => $this->input->post('email'),
			'gender' => $this->input->post('gender'),'role_id' => $this->input->post('role_id'),
			'password' => $encrypted_password
			//'confpwd' => $repeat_pass
		);
			$this->load->model('queries');	
			if($this->queries->registerAdmin($data)){ // if data entered successfully return a value of 1
				 $this->session->set_flashdata('message','Admin Registered Successfully');
				 return redirect("welcome/login");
			}	
			else{
				$this->session->set_flashdata('message','Fail to Register Admin');
				 return redirect("welcome/adminRegister");


			}
		}
		
		else{
			$this->adminRegister();
	}
	
}
   public function login(){
	   if($this->session->userdata("user_id"))
	   return redirect("admin/dashboard");
	   $this->load->view('login');
   }

   //admin login
   public function signin(){
	//$this->form_validation->set_rules('username','Username','required');
	$this->form_validation->set_rules('email','Email','required');
	$this->form_validation->set_rules('password','Password','required');
	$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
	if($this->form_validation->run()){
		$this->load->library('encryption');

		  // $username = $this->input->post('username');
		  $email =$this->input->post('email');
		  $password= hash('sha256', 'x(93g'.$this->input->post("password").'4$b7@');
		  $this->load->model('queries');
		  $userExist = $this->queries->logme($email,$password);
		 
		  if($userExist){
			  $sessionData = [
				  'user_id' => $userExist->user_id,
				  'username' => $userExist->username,
				  'email' => $userExist->email,
				  'password'=>$userExist->password,    // add pass to session
				  'role_id' => $userExist->role_id,
			  ];
			  $this->session->set_userdata($sessionData);
			  return redirect("admin/dashboard");
		  }
		  else{
			   $this ->session->set_flashdata('message',  'Your Email or Password is not incorrect');
			   return redirect("welcome/login");
		  }
	}
	else{
           $this->login();
	}
   }

   public function logout(){
	   $this->session->unset_userdata("user_id");
	   return redirect("welcome/login");

   }

  


      // form control for adding computers
   public function deviceDetails(){
		
	$this->form_validation->set_rules('device_name','Device Name','required');
	$this->form_validation->set_rules('processor','Processor','required');
	$this->form_validation->set_rules('ram','Ram','required');
	$this->form_validation->set_rules('os_type','Operating System','required');
	$this->form_validation->set_rules('hardrive_size','Hard Drive Size','required');
	$this->form_validation->set_rules('parish_id','Location','required');
	$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

	if($this->form_validation->run() ){

		$data =array('device_name' => $this->input->post('device_name'),'processor' => $this->input->post('processor'),
		'ram' => $this->input->post('ram'),'os_type' => $this->input->post('os_type'),
		'hardrive_size' => $this->input->post('hardrive_size'),'parish_id' => $this->input->post('parish_id')
	);
		$this->load->model('queries');	
		if($this->queries->registerComputer($data)){ 
			 $this->session->set_flashdata('message','Computer Added Successfully');
			 return redirect("admin/dashboard");
		}	
		else{
			$this->session->set_flashdata('message','Fail to Register Computer');
			 return redirect("welcome/deviceDetails");

		}
	}
	
	else{
		$this->computerLocations();
}
   }

   


  


}


