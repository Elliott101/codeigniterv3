<?php

class Queries extends CI_Model{

    public function getRoles(){
        $roles=$this->db->get('tbl_roles');
        if($roles->num_rows()>0){
            return $roles->result();
        }
    }

    public function getColleges(){
        $colleges=$this->db->get('tbl_college');
        if($colleges->num_rows()>0){
            return $colleges->result();
        }
    }

    //---------------------------------------
public function getLocation(){
  $parishes=$this->db->get('tbl_parishes');
  if($parishes->num_rows()>0){
      return $parishes->result();
  }
}






public function registerAdmin($data){
    return $this->db->insert('tbl_users',$data); // inserting data into users table
}



public function chkAdminExist(){

    $chkAdmin = $this->db->where(['role_id' => '1'] )->get('tbl_users');
    if($chkAdmin->num_rows()>0){
        return $chkAdmin->num_rows();
    }
}


     // if admin exist login user
    public function adminExist($email, $password){ 
     $chkAdmin = $this-> db-> where([ 'password' =>$password,'username ' =>$email])
      ->get('tbl_users');
      
      if($chkAdmin ->num_rows()>0){
          return  $chkAdmin->row();
      }

    }
    public function makeCollege($data){
        return $this->db->insert('tbl_college',$data);
    }

    public function registerCoadmin($data){
        return $this->db->insert('tbl_users',$data);

    }


// Checking Username method 


    function checkusername($username)
 {
   $this -> db -> select('username');
   $this -> db -> from('tbl_users');
   $this -> db -> where('username', $username);
   $this -> db -> limit(1);
 
   $query = $this -> db -> get();
 
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 function checkMail($email)
 {
   $this -> db -> select('email');
   $this -> db -> from('tbl_users');
   $this -> db -> where('email', $email);
   $this -> db -> limit(1);
 
   $query = $this -> db -> get();
 
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 //function to login
 function logme($email,$password){

  $this -> db -> select('user_id','username','email','password','role_id');
  $this -> db -> from('tbl_users');
  $this->db->where('email',$email);
  $this->db->or_where('username',$email);
  $this->db->where('password',$password);
  $this -> db -> limit(1);
 
  $query = $this -> db -> get();

  if($query -> num_rows() == 1)
  {
    return $query->row();
  }
  else
  {
    return false;
  }

 }

 public function registerComputer($data){
  return $this->db->insert('tbl_computers',$data); // inserting data into computers table
}

/*
public function viewAllColleges(){
$this->db->select(['tbl_users.user_id','tbl_college.college_id','tbl_users.username','tbl_users.gender',
'tbl_college.collegename','tbl_college.branch','tbl_roles.rolename']);
$this->db->from('tbl_college');
$this->db->join('tbl_users','tbl_users.college_id = tbl_college.college_id');
$this->db->join('tbl_roles','tbl_roles.role_id = tbl_users.role_id');
$users = $this->db->get();
return $users->result();
} $device_name, $processor, $ram,$os_type,$hardrive_size
*/

public function viewAllComputers(){ //generate computer
  $this->db->select(['tbl_computers.device_id','tbl_computers.device_name','tbl_computers.processor','tbl_computers.ram','tbl_computers.os_type',
  'tbl_computers.hardrive_size','tbl_parishes.parish_name']);
  $this->db->from('tbl_computers');
  $this->db->join('tbl_parishes','tbl_parishes.parish_id=tbl_computers.parish_id');
  $users = $this->db->get();
  return $users->result();
}

public function getComputerRecord($device_id){  // editing computer record
  $this->db->select(['tbl_computers.device_id','tbl_computers.device_name','tbl_computers.processor','tbl_computers.ram'
  ,'tbl_computers.os_type','tbl_computers.hardrive_size']);
$this->db->from('tbl_computers');
$this->db->where(['tbl_computers.device_id'=>$device_id]);
$computer = $this->db->get();
return $computer->row();

}

public function updateComputer($data,$device_id){
  return $this->db->where('device_id',$device_id)->update('tbl_computers',$data);
}

public function removeComputer($device_id){
  return $this->db->delete('tbl_computers',['device_id'=>$device_id]);

}

//------------------------------------
public function getParishes(){
  $query = $this->db->get('tbl_parishes');
  if($query->num_rows()>0){
    return $query->result();
  }
}

public function getUsers(){
  $query = $this->db->get('tbl_users');
  if($query->num_rows()>0){
    return $query->result();
  }
}

  public function getRecords($parish){
       
    $this->db->select(['tbl_computers.device_id','tbl_computers.device_name','tbl_computers.processor','tbl_computers.ram','tbl_computers.os_type',
  'tbl_computers.hardrive_size','tbl_parishes.parish_name']); // 'tbl_parishes.parish_id as parishID'
    $this->db->from('tbl_computers');
    $this->db->join('tbl_parishes','tbl_parishes.parish_id=tbl_computers.parish_id');
    $this->db->where(['tbl_computers.parish_id'=>$parish]);
    $query = $this->db->get();
    return $query->result();



  }






}





?>