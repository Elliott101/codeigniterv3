<?php include("inc/header.php"); ?>


<div class ="container">


        <h3>Admin Dashboard </h3>
      <?php $username= $this->session->userdata('username'); ?>
    
   

        <?php echo anchor ("admin/addCollege" , "ADD COLLEGE", ['class'=> 'btn btn-primary']); ?>
        <?php echo anchor ("admin/addCoAdmin" , "ADD COADMIN", ['class'=> 'btn btn-primary']); ?>
        <?php echo anchor ("admin/testButton" , "TESTING", ['class'=> 'btn btn-primary']); ?>
        <?php echo anchor ("welcome/deviceDetails" , "ADD COMPUTER", ['class'=> 'btn btn-success']); ?>
        <?php echo anchor ("welcome/filterComputers" , "FILTER BY LOCATION", ['class'=> 'btn btn-danger']); ?>




        <hr> 
        <div class="row">
            <table class="table table-hover table-light ">
                <thead>
                    <tr class="table-danger">
                       <th scope= "col"> DEVICE_ID </th> 
                       <th scope= "col"> DEVICE_NAME </th> 
                       <th scope= "col"> DEVICE_PROCESSOR</th> 
                       <th scope= "col"> DEVICE_RAM</th> 
                       <th scope= "col"> OPERATING_SYSTEM </th>
                       <th scope= "col"> HARDRIVE_SIZE</th> 
                       <th scope= "col"> LOCATION</th> 
                       <th scope= "col"> ACTION</th> 
                     
                       
                      
                        
                       
                    </tr>
                </thead>
                <tbody>
                <?php if(count($users)): ?>
                <?php foreach($users as $user): ?>
                    <tr class ="table-active table-hover table-primary table-bordered">
                        <td><?php echo $user->device_id; ?></td>
                        <td><?php echo $user->device_name; ?></td>
                        <td><?php echo $user->processor; ?></td>
                        <td><?php echo $user->ram; ?></td>
                        <td><?php echo $user->os_type; ?></td>
                        <td><?php echo $user->hardrive_size; ?></td>
                        <td><?php echo $user->parish_name; ?></td>
                       
                        <td> 
                        <?php echo anchor ("admin/editComputer/{$user->device_id}" , "Edit", ['class'=> 'btn btn-success']);  ?>
                        <button type="button" class="btn btn-danger" class="bi bi-trash" data-toggle="modal" data-target="#exampleModalCenter">
                        <i class="fas fa-trash-alt"></i>
                      Delete
                        </button>
                        <i class="bi bi-trash"></i>
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Are You Sure You Want To Delete?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       ...
      </div>
      <div class="modal-footer">
      <?php echo anchor ("admin/dashboard" , "No", ['class'=> 'btn btn-outline-success']); ?>
        <?php echo anchor ("admin/deleteComputer/{$user->device_id}" , "Yes", ['class'=> 'btn btn-outline-danger'])?>
      </div>
    </div>
  </div>
</div>

                       
                        
                        
                        
                        </td>
                        
                    </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                    <td>No Record Found!!!</td>
                    </tr>
                        <?php endif; ?>
                </tbody>

            </table>
        </div>

</div>


<?php include("inc/footer.php"); ?>