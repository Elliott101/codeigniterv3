<?php include("inc/header.php"); ?>

<div class ="container">

<?php echo form_open("welcome/getRecords");?>
<select name ="parish_id">
<option value="">Select Parish</option>
<?php if(count($getParishes)):?>  <!-- coming from wecomputerLocations() controller-->
    <?php foreach($getParishes as $parish):?>
    <option value=<?php echo $parish->parish_id?>>
    <?php echo $parish->parish_name?></option>
   <?php endforeach;?>
  <?php endif;?>                     
</select>
<?php echo form_submit(['name'=>'submit','value'=>'Filter']);?>
<?php echo form_close();?>

<br/>

<div class="row">
            <table class="table table-hover table-light ">
                <thead>
                    <tr class="table-danger">
                       <th scope= "col"> DEVICE_ID </th> 
                       <th scope= "col"> DEVICE_NAME </th> 
                       <th scope= "col"> DEVICE_PROCESSOR</th> 
                       <th scope= "col"> DEVICE_RAM</th> 
                       <th scope= "col"> OPERATING_SYSTEM </th>
                       <th scope= "col"> HARDRIVE_SIZE</th> 
                       <th scope= "col"> LOCATION</th> 
                      
                     
                       
                      
                        
                       
                    </tr>
                       
                      
                        
</tr>

               <?php if(count($records)): ?>
                <?php foreach($records as $record): ?>
                    <tr class ="table-active table-hover table-primary table-bordered">
                        <td><?php echo $record->device_id; ?></td>
                        <td><?php echo $record->device_name; ?></td>
                        <td><?php echo $record->processor; ?></td>
                        <td><?php echo $record->ram; ?></td>
                        <td><?php echo $record->os_type; ?></td>
                        <td><?php echo $record->hardrive_size; ?></td>
                        <td><?php echo $record->parish_name; ?></td>
<tr>
<td></td>
</tr>

</tr>
                     
                        
                        </tr>
                        <?php endforeach; ?>
                        <?php else: ?>
                        <tr>
                        <td>No Record Found!!!</td>
                        </tr>
                            <?php endif; ?>
                    </tbody>
    
                </table>
            </div>
    
    </div>
    </div>
    

<?php include("inc/footer.php"); ?>