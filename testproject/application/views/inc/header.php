<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js">
</head> 
<body>

 	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
 		<div class= "container-fluid">
 			<div class="navbar-header col-lg-10">
 				<a href="" class= "navbar-brand">HIGH SCHOOL MANAGEMENT SYSTEM </a>
 			</div>
 		<div class = "col-lg-2" style="margin-top:15px;" id="bs-example-navbar-collapse-2">
 			<div class="dropdown btn-group" >
	 		<a href="#" class="btn btn-primary"  style="color:white;">Settings</a>
			 <a href="#" class="btn btn-primary dropdown-toggle"  style="color:white;" data-toggle="dropdown" aria-expanded="false"> 
			 </a>
			 <span class="caret"></span>
				<ul class="dropdown-menu" style="color:aqua;">
					<li><?php echo anchor("admin/dashboard",'Dashboard')    ?></li>
					<li><?php echo anchor("welcome/logout",'Logout')    ?></li>
				</ul>
			</div>
		</div>
	</div>
 </nav>

</body>












