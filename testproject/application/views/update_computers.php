<?php include("inc/header.php"); ?>

<div class="container">
    <?php echo form_open("welcome/deviceDetails",['class'=>'form-horizontal']); ?>
    <?php if($msg = $this->session->flashdata('message')):?>
        <div class="row">
            <div class ="col-md-6">
                <div class="alert alert-dismissable alert-success">
                     <?php echo $msg; ?>
                    
                    </div>

            </div>
        </div>
    <?php endif; ?>

<h3>Enter Computer Details</h3>
<hr>
<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Device Name</label>
                <div class="col-md-9">
                    <?php echo form_input(['name'=>'device_name', 'class'=>'form-control',
                    'placeholder'=>'','value'=>set_value('device_name')]); ?>
                </div>
            </div>
       </div>
    <div class="col-md-6">   
    <?php echo form_error('device_name','<div class="text-danger">','</div>');?>
    </div>
</div>
<!--------------------------------------->
<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Type of Processor</label>
                <div class="col-md-9">
                    <?php echo form_input(['name'=>'processor', 'class'=>'form-control',
                    'placeholder'=>'','value'=>set_value('processor')]); ?> <!-- set value makes it so data stays in form-->
                </div>
            </div>
    </div>
    <div class="col-md-6">
    <?php echo form_error('processor','<div class="text-danger">','</div>');?>
    </div>
</div>

<!-------------------------------------------->

<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label"> RAM </label>
                <div class="col-md-9">
                    <?php echo form_input(['name'=>'ram', 'class'=>'form-control',
                    'placeholder'=>'','value'=>set_value('ram')]); ?>
                </div>
            </div>
       </div>
    <div class="col-md-6">   
    <?php echo form_error('ram','<div class="text-danger">','</div>');?>
    </div>
</div>
<!--------------------------------------------------->
<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label white-space: nowrap;"> Operating System </label>
                <div class="col-md-9">
                    <?php echo form_input(['name'=>'os_type', 'class'=>'form-control',
                    'placeholder'=>'','value'=>set_value('os_type')]); ?>
                </div>
            </div>
       </div>
    <div class="col-md-6">   
    <?php echo form_error('os_type','<div class="text-danger">','</div>');?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label"> Hard Drive Size </label>
                <div class="col-md-9">
                    <?php echo form_input(['name'=>'hardrive_size', 'class'=>'form-control',
                    'placeholder'=>'','value'=>set_value('hardrive_size')]); ?>
                </div>
            </div>
       </div>
    <div class="col-md-6">   
    <?php echo form_error('hardrive_size','<div class="text-danger">','</div>');?>
    </div>
</div>





<button type="submit" class="btn btn-primary">ADD Computer</button>
<?php echo anchor("admin/dashboard","BACK",['class' =>'btn btn-primary']); ?>




<?php echo form_close(); ?>
</div>
<?php include("inc/footer.php"); ?>






